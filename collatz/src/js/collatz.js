let renderer, camera, scene, container;
let controls;
let line;

function setup() {

    container = document.getElementById("container");

    // CAMERA
    camera = new THREE.PerspectiveCamera(50, window.innerWidth / window.innerHeight, 0.01, 1000);
    camera.position.set(0, 50, 500);

    // SCENE
    scene = new THREE.Scene();
    scene.background = new THREE.Color(0x00000);
    scene.add(camera);

    // LIGHTS
    let light = new THREE.DirectionalLight(0xffffff);
    light.position.set(0, 1, 1);
    scene.add(light);

    // CURVE EXAMPLE
    // drawTubeCurve();

    drawAxis(100, 0.002);
    drawGrid(200, 200, 8);

    // SPLINE EXAMPLE
    let curvePoints = new THREE.CatmullRomCurve3([
        new THREE.Vector3(-10, 0, 10),
        new THREE.Vector3(-5, 5, 5),
        new THREE.Vector3(-0, 0, 0),
        new THREE.Vector3(5, -5, 5),
        new THREE.Vector3(10, 0, 10)
    ]);
    curvePoints.closed = false;
    curvePoints.curveType = 'catmullrom';

    let extrusionSettings = {
        steps: 100,
        bevelEnabled: true,
        extrudePath: curvePoints
    };

    let points = [];
    let count = 32;

    for (let i = 0; i < count; i++) {
        let radius = 1;
        let angle = 2 * i / count * Math.PI;
        points.push(new THREE.Vector2(Math.cos(angle) * radius, Math.sin(angle) * radius));
    }

    let shape = new THREE.Shape(points);
    let geometry = new THREE.ExtrudeGeometry(shape, extrusionSettings);
    let material = new THREE.MeshLambertMaterial({
        color: 0xff0000,
        wireframe: false
    });
    let mesh = new THREE.Mesh(geometry, material);
    mesh.position.y += 20;
    // mesh.geometry.mergeVertices();
    // mesh.geometry.computeVertexNormals();
    scene.add(mesh);

    // PLANE
    let planeGeo = new THREE.PlaneBufferGeometry(200, 200, 8, 8);
    let planeMat = new THREE.MeshLambertMaterial({
        color: 0xffffff,
        side: THREE.DoubleSide,
        wireframe: true
    });
    let plane = new THREE.Mesh(planeGeo, planeMat);
    plane.rotateX(-Math.PI / 2);
    scene.add(plane);

    // RENDERER
    renderer = new THREE.WebGLRenderer({
        antialias: true
    });
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(window.innerWidth, window.innerHeight);

    // CONTROLS
    controls = new THREE.OrbitControls(camera, renderer.domElement);
    controls.minDistance = 20;
    controls.maxDistance = 1500;

    container.appendChild(renderer.domElement);
    window.addEventListener('resize', onWindowResize, false);
}

function update() {
    requestAnimationFrame(update);
    controls.update();
    render();
}

function render() {
    renderer.render(scene, camera);
}

// LISTENERS
function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight);
}

// CUSTOM STUFF
function drawGrid(sizeX, sizeY, resolution) {

    let planeGeo = new THREE.PlaneBufferGeometry(sizeX, sizeY, resolution, resolution);
    let planeMat = new THREE.MeshLambertMaterial({
        color: 0xffffff,
        side: THREE.DoubleSide,
        wireframe: true
    });
    let plane = new THREE.Mesh(planeGeo, planeMat);
    plane.rotateX(-Math.PI / 2);
    scene.add(plane);
}

function drawAxis(size, lineWidth) {

    let xPositions = [],
        yPositions = [],
        zPositions = [];

    // create the geos
    let lineGeoX = new THREE.LineGeometry(),
        lineGeoY = new THREE.LineGeometry(),
        lineGeoZ = new THREE.LineGeometry();

    for (let i = 0; i < 2; i++) {
        xPositions.push(i * size, 0, 0);
        yPositions.push(0, i * size, 0);
        zPositions.push(0, 0, i * size);
    }

    // materials
    let xMat = new THREE.LineMaterial({
        color: 0xff0000,
        linewidth: lineWidth, // in pixels
        dashed: false
    });
    let yMat = new THREE.LineMaterial({
        color: 0x00ff00,
        linewidth: lineWidth, // in pixels
        dashed: false
    });
    let zMat = new THREE.LineMaterial({
        color: 0x0000ff,
        linewidth: lineWidth, // in pixels
        dashed: false
    });

    // meshes
    let lineMeshX = new THREE.Line2(lineGeoX, xMat);
    let lineMeshY = new THREE.Line2(lineGeoY, yMat);
    let lineMeshZ = new THREE.Line2(lineGeoZ, zMat);
    lineGeoX.setPositions(xPositions);
    lineGeoY.setPositions(yPositions);
    lineGeoZ.setPositions(zPositions);

    scene.add(lineMeshX);
    scene.add(lineMeshY);
    scene.add(lineMeshZ);
}

function createTubeCurve() {
    let curve = new THREE.SplineCurve3([
        new THREE.Vector3(-10, 0, 10),
        new THREE.Vector3(-5, 5, 5),
        new THREE.Vector3(-0, 0, 0),
        new THREE.Vector3(5, -5, 5),
        new THREE.Vector3(10, 0, 10)
    ]);
    let hSegments = 32;
    let vSegments = 16;
    let radius = 1;
    let tubeGeo = new THREE.TubeGeometry(curve, hSegments, radius, vSegments, true);
    let tubeMat = new THREE.MeshLambertMaterial({
        color: 0xff0000
    });
    let tubeMesh = new THREE.Mesh(tubeGeo, tubeMat);
    tubeMesh.position.y += 10;
    scene.add(tubeMesh);
}

/**
 * In Javascript there's no map function as in ofMap, so I'm using adding it
 * original oo implementation by https://gist.github.com/xposedbones/75ebaef3c10060a3ee3b246166caab56
 * @param {n} - the input number to map
 * @param {inMin} - the minimum value of the input interval
 * @param {inMax} - the maximum value of the input interval
 * @param {outMin} - the minimum value of the output interval
 * @param {outMax} - the maximum value of the output interval
 */
function fitRange(n, inMin, inMax, outMin, outMax) {
    return (n - inMin) * (outMax - outMin) / (inMax - inMin) + outMin;
}

/**
 * creates a tree like structure using collatz
 * @param {n} - the number of wich compute the collatz
 * @param {branchLength} - the length of the generated branch
 * @param {radAngles} - an object containing the x and y angles (in radians) to be used to turn left/right.
 */
function collatzTree(n, branchLength, radAngles) {

    let collatzNumbers = [];
    let treeGeo = new THREE.BufferGeometry();

    let positions = new Float32Array(512 * 3);
    treeGeo.addAttribute('position', new THREE.BufferAttribute(positions, 3));

    // line material
    let material = new THREE.LineBasicMaterial({
        color: 0xff0000
    });

    // actual line
    let line = new THREE.Line(treeGeo, material);

    if (n === 1) {
        return collatzNumbers;
    }
    // turn left or right depending on number parity
    else if (n % 2 == 0) {
        n = n / 2;
    } else {
        n = n * 3 + 1;
    }
}

setup();
update();